DROP TABLE public."Area"
IF EXISTS;
DROP TABLE public."Assign"
IF EXISTS;
DROP TABLE public."Employee"
IF EXISTS;
DROP TABLE public."Lifeguard"
IF EXISTS;
DROP TABLE public."Pool"
IF EXISTS;
DROP TABLE public."Week"
IF EXISTS;
CREATE SEQUENCE "hibernate_sequence"
    START WITH 1
    INCREMENT BY 1;
CREATE TABLE public."Employee"
(
  id   INTEGER                NOT NULL,
  name CHARACTER VARYING(128) NOT NULL,
  role CHARACTER(1),
  CONSTRAINT "EmployeeId" PRIMARY KEY (id)
);
CREATE TABLE public."Area"
(
  id         INTEGER NOT NULL,
  state      CHARACTER(2),
  supervisor INTEGER,
  name       CHARACTER VARYING(128),
  CONSTRAINT "AreaId" PRIMARY KEY (id),
  CONSTRAINT "SupervisorId" FOREIGN KEY (supervisor)
  REFERENCES public."Employee" (id)
    MATCH SIMPLE
    ON UPDATE SET NULL
    ON DELETE SET NULL
);

CREATE TABLE public."Week"
(
  id       INTEGER NOT NULL,
  "number" INTEGER,
  start    DATE,
  CONSTRAINT "WeekId" PRIMARY KEY (id)
);
CREATE TABLE public."Lifeguard"
(
  id                       INTEGER                NOT NULL,
  name                     CHARACTER VARYING(100) NOT NULL,
  country                  CHARACTER VARYING(100),
  appartments              CHARACTER VARYING(256),
  area_id                     INTEGER,
  average_hours_per_season DOUBLE PRECISION,
  CONSTRAINT "LifeguardId" PRIMARY KEY (id),
  CONSTRAINT "AreaID" FOREIGN KEY (area)
  REFERENCES public."Area" (id)
    MATCH SIMPLE
    ON UPDATE SET NULL
    ON DELETE SET NULL
);
CREATE TABLE public."Pool"
(
  id        INTEGER                NOT NULL,
  name      CHARACTER VARYING(100) NOT NULL,
  address   CHARACTER VARYING(256) NOT NULL,
  lg_count  SMALLINT,
  area_id      INTEGER,
  mon_start TIME WITHOUT TIME ZONE NOT NULL,
  mon_end   TIME WITHOUT TIME ZONE NOT NULL,
  tue_start TIME WITHOUT TIME ZONE NOT NULL,
  tue_end   TIME WITHOUT TIME ZONE NOT NULL,
  wed_start TIME WITHOUT TIME ZONE NOT NULL,
  wed_end   TIME WITHOUT TIME ZONE NOT NULL,
  thu_start TIME WITHOUT TIME ZONE NOT NULL,
  thu_end   TIME WITHOUT TIME ZONE NOT NULL,
  fri_start TIME WITHOUT TIME ZONE NOT NULL,
  fri_end   TIME WITHOUT TIME ZONE NOT NULL,
  sat_start TIME WITHOUT TIME ZONE NOT NULL,
  sat_end   TIME WITHOUT TIME ZONE NOT NULL,
  sun_start TIME WITHOUT TIME ZONE NOT NULL,
  sun_end   TIME WITHOUT TIME ZONE NOT NULL,
  CONSTRAINT "PoolId" PRIMARY KEY (id),
  CONSTRAINT "Area" FOREIGN KEY (area)
  REFERENCES public."Area" (id)
    MATCH SIMPLE
    ON UPDATE SET NULL
    ON DELETE SET NULL
);
);
CREATE TABLE public."Assign"
(
  id           INTEGER                NOT NULL,
  pool_id      INTEGER                NOT NULL,
  lifeguard_id INTEGER                NOT NULL,
  time_in      TIME WITHOUT TIME ZONE NOT NULL,
  time_out     TIME WITHOUT TIME ZONE NOT NULL,
  assign_date  DATE,
  CONSTRAINT "AssignId" PRIMARY KEY (id),
  CONSTRAINT "Lifeguard" FOREIGN KEY (lifeguard_id)
  REFERENCES public."Lifeguard" (id)
    MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT "Pool" FOREIGN KEY (pool_id)
  REFERENCES public."Pool" (id)
    MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
INSERT INTO public."Lifeguard" VALUES (1, 'Roman Diachuk', 'Ukraine', 'Courts of Mount Vernon');
INSERT INTO public."Lifeguard" VALUES (2, 'Valentin Ovcharov', 'Ukraine', 'Courts of Mount Vernon');
INSERT INTO public."Lifeguard" VALUES (3, 'Paul Sorokovskiy', 'Ukraine', 'Courts of Mount Vernon');
INSERT INTO public."Pool" VALUES
  (1, 'River farm', 'Holosiivskiy ave', 2, '10:00:00', '20:00:00', '10:00:00', '20:00:00', '10:00:00', '20:00:00',
      '10:00:00', '20:00:00', '10:00:00', '22:00:00', '10:00:00', '22:00:00', '10:00:00', '20:00:00');
INSERT INTO public."Pool" VALUES
  (2, 'Oaks Of Woodlawn', 'st Pierre,21', 1, '12:00:00', '20:00:00', '12:00:00', '20:00:00', '12:00:00', '20:00:00',
      '12:00:00', '20:00:00', '12:00:00', '20:00:00', '12:00:00', '20:00:00', '12:00:00', '20:00:00');
INSERT INTO public."Assign" VALUES (1, 1, 2, '09:30:00', '20:00:00', '23.02.2017');
INSERT INTO public."Assign" VALUES (2, 1, 3, '10:00:00', '20:00:00', '24.02.2017');
INSERT INTO public."Assign" VALUES (3, 2, 1, '12:00:00', '20:00:00', '25.02.2017');
COMMIT;