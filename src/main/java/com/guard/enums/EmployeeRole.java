package com.guard.enums;

public enum EmployeeRole {
    SUPERVISOR,
    MANAGER;
}
