package com.guard.enums;

public enum Days {
    MONDAY("mon"),
    TUESDAY("tue"),
    WEDNESDAY("wed"),
    THURSDAY("thu"),
    FRIDAY("fri"),
    SATURDAY("sat"),
    SUNDAY("sun");

    final private String shortRepresentation;

    Days(String shortRepresentation) {
        this.shortRepresentation = shortRepresentation;
    }

    public String getShortRepresentation(){
        return shortRepresentation;
    }
}
