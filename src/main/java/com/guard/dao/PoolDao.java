package com.guard.dao;

import com.guard.model.Area;
import com.guard.model.Pool;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class PoolDao extends AbstractDao<Pool> {

    public Pool getByName(String name) {
        return getByField("name",name).get(0);
    }

    public List<Pool> getAllForArea(Area area){
        Query query = entityManager.createQuery("from Pool pool where ((:area is null and pool.area is null) or pool.area=:area )");
        query.setParameter("area", area);
        return query.getResultList();
    }
    public List<Pool> getAllForArea(int areaId){
        Query query = entityManager.createQuery("from Pool pool join pool.area area where ((:area is null and pool.area is null) or pool.area.id=:area )");
        query.setParameter("area", areaId);
        return query.getResultList();
    }

}
