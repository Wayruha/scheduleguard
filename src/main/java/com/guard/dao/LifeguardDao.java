package com.guard.dao;

import com.guard.model.Area;
import com.guard.model.Assign;
import com.guard.model.Lifeguard;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class LifeguardDao extends AbstractDao<Lifeguard> {

    public Lifeguard getByName(String name) {
        return getByField("name", name).get(0);
    }

    public List<Lifeguard> getAllForArea(Area area) {
        Query query = entityManager.createQuery("from Lifeguard lg where ((:area is null and lg.area is null) or lg.area=:area )");
        query.setParameter("area", area);
        return query.getResultList();
    }

    public List<Lifeguard> getAllForArea(int areaId) {
        Query query = entityManager.createQuery("from Lifeguard lg join lg.area area where ((:area is null and lg.area is null) or lg.area.id=:area )");
        query.setParameter("area", areaId);
        return query.getResultList();
    }

    //TODO Як краще, знайти ентіті в бд і потім emManager.remove чи самому запит писати?
    public boolean deleteById(int id) {
        boolean result = false;
        try {
            Query query = entityManager.createQuery("delete from Lifeguard lg WHERE  lg.id=:id");
            query.setParameter("id", id);
            query.executeUpdate();
            result = true;
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return result;
    }

    final Date startOfSeason;
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.DATE, 20);
        calendar.set(Calendar.YEAR, 2017);
        calendar.getTime();
        startOfSeason = calendar.getTime();
    }

}
