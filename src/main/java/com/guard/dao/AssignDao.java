package com.guard.dao;

import com.guard.model.Assign;
import com.guard.model.Lifeguard;
import com.guard.model.Pool;
import com.guard.model.Schedule;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class AssignDao extends AbstractDao<Assign> {


    public Schedule<Lifeguard> getScheduleForLifeguard(Lifeguard lg, Date startOfWeek) {
        List<Assign> assigns = getAssignsForLifeguard(lg, startOfWeek);
        return new Schedule<>(assigns);
    }

    public Schedule<Pool> getScheduleForPool(Pool pool, Date startOfWeek) {
        List<Assign> assigns = getPoolSchedule(pool, startOfWeek);
        return new Schedule<>(assigns);
    }


    public List<Assign> getAssignsForLifeguard(Lifeguard lg, Date startOfWeek) {
        Query query;
        if (startOfWeek == null)
            query = entityManager.createQuery("from Assign asn where asn.lifeguard=:lg");
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startOfWeek);
            calendar.add(Calendar.WEEK_OF_YEAR, 1);
            Date endOfWeek = calendar.getTime();

            query = entityManager.createQuery("from Assign asn where asn.lifeguard=:lg and asn.date >= :startOfWeek and asn.date < :endOfWeek");
            query.setParameter("startOfWeek", startOfWeek);
            query.setParameter("endOfWeek", endOfWeek);
        }
        query.setParameter("lg", lg);
        return query.getResultList();
    }


    public List<Assign> getPoolSchedule(Pool pool, Date startOfWeek) {
        Query query;
        if (startOfWeek == null)
            query = entityManager.createQuery("from Assign asn where asn.pool=:pool");
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startOfWeek);
            calendar.add(Calendar.WEEK_OF_YEAR, 1);
            Date endOfWeek = calendar.getTime();

            query = entityManager.createQuery("from Assign asn where asn.pool=:pool and asn.date >= :startOfWeek and asn.date < :endOfWeek");
            query.setParameter("startOfWeek", startOfWeek);
            query.setParameter("endOfWeek", endOfWeek);
        }
        query.setParameter("pool", pool);
        return query.getResultList();
    }

    public List<Assign> getAssignsForLifeguard(Lifeguard lg) {
        return getAssignsForLifeguard(lg, null);
    }

    public List<Assign> getPoolSchedule(Pool pool) {
        return getPoolSchedule(pool, null);
    }

}
