package com.guard.dao;

import com.guard.model.Area;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class AreaDao extends AbstractDao<Area>{
}
