package com.guard.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Objects;

//@Transactional
public abstract  class AbstractDao<T> {

    @PersistenceContext
    protected EntityManager entityManager;  //Можна писати запити через sessions, а можна через ентіті менеджер. не впевнений шо краще

    private final Class<T> persistentClass;


    public AbstractDao(){
        this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void save(T entity){
        entityManager.persist(entity);

    }
    public void delete(T entity){
        entityManager.remove(entity);
    }

    public T getById(int id){
        return (T)entityManager.createQuery("select c from "+persistentClass.getCanonicalName()+" c where c.id=:id")
                .setParameter("id",id).getSingleResult();
    }

    public List<T> getAll(){
         return entityManager.createQuery("select c from "+persistentClass.getCanonicalName()+" c")
                 .getResultList();
    }

    public List<T> getByField(String fieldName, String value){
        return entityManager.createQuery("select c from "+persistentClass.getCanonicalName()+" c WHERE c."+fieldName+"=:fValue")
                .setParameter("fValue",value)
                .getResultList();
    }

    public List<T> getByField(String fieldName, int value){
        return entityManager.createQuery("select c from "+persistentClass.getCanonicalName()+" c WHERE c."+fieldName+"=:fValue")
                .setParameter("fValue",value)
                .getResultList();
    }

    public T updateRecord(T entity){
        return entityManager.merge(entity);
    }

}
