package com.guard.validatingForm;

import com.guard.model.Lifeguard;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class LifeguardForm {

    @Length(max = 10, min = 2)
    private String name;
    private String appartments;
    private String country;
    @Min(1)
    @Max(10)
    private int area;


    public LifeguardForm() {
    }

    public LifeguardForm(Lifeguard lg) {
        this.name = lg.getName();
        this.appartments = lg.getAppartments();
        this.country = lg.getCountry();
        if (lg.getArea() != null) this.area = lg.getArea().getId();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppartments() {
        return appartments;
    }

    public void setAppartments(String appartments) {
        this.appartments = appartments;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }
}
