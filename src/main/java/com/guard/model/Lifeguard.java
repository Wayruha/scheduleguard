package com.guard.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.guard.validatingForm.LifeguardForm;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "\"Lifeguard\"")
public class Lifeguard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private String name;
    @Column
    private String country;
    @Column
    private String appartments;
    @ManyToOne
    @JoinColumn(name = "area_id")
    private Area area;

    @JsonIgnore
    @OneToMany(mappedBy = "lifeguard", fetch = FetchType.LAZY)
    private Set<Assign> assignSet;

    @Transient
    private double hoursPerCurrentWeek;
    @Column(name = "average_hours_per_season")
    private Double averageHours;

    public Lifeguard() {
    }

    public Lifeguard(LifeguardForm lgForm, Area area) {
        this.name = lgForm.getName();
        this.appartments = lgForm.getAppartments();
        this.country = lgForm.getCountry();
        this.area = area;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Assign> getAssignSet() {
        return assignSet;
    }

    public void setAssignSet(Set<Assign> assignSet) {
        this.assignSet = assignSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getAppartments() {
        return appartments;
    }

    public void setAppartments(String appartments) {
        this.appartments = appartments;
    }

    public double getHoursPerCurrentWeek() {
        return hoursPerCurrentWeek;
    }

    public void setHoursPerCurrentWeek(double hoursPerCurrentWeek) {
        this.hoursPerCurrentWeek = hoursPerCurrentWeek;
    }

    public Double getAverageHours() {
        return averageHours;
    }

    public void setAverageHours(Double averageHours) {
        this.averageHours = averageHours;
    }

    @Override
    public String toString() {
        return "[lg#" + id + "] " + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lifeguard)) return false;

        Lifeguard lifeguard = (Lifeguard) o;

        return id == lifeguard.id;
    }

    @Override
    public int hashCode() {
        return id;
    }


}
