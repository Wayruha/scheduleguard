package com.guard.model;

import com.guard.enums.EmployeeRole;

import javax.persistence.*;


@Entity
@Table(name = "\"Employee\"")
public class Employee {

    @Id
    @GeneratedValue
    private int id;
    @Column
    private String name;
    @Column
    @Enumerated(EnumType.STRING)
    private EmployeeRole role; //TODO enumeration


    public Employee() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmployeeRole getRole() {
        return role;
    }

    public void setRole(EmployeeRole role) {
        this.role = role;
    }
}
