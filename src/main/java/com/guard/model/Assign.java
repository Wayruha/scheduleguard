package com.guard.model;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name = "\"Assign\"")
public class Assign {

    @Id
    @GeneratedValue
    private int id;
    @ManyToOne
    @JoinColumn(name = "pool_id", nullable = false)
    private Pool pool;
    @ManyToOne
    @JoinColumn(name = "lifeguard_id", nullable = false)
    private Lifeguard lifeguard;
    @Column(name = "time_in", nullable = false)
    private LocalTime timeIn;
    @Column(name = "time_out", nullable = false)
    private LocalTime timeOut;
    @Column(name = "assign_date")
    @Temporal(TemporalType.DATE)
    private Date date;


    //day
    //week
    public Assign() {

    }

    //TODO округляти до 0.5 години
    public double getHoursCount() {
        return Duration.between(timeIn,timeOut).toHours();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pool getPool() {
        return pool;
    }

    public void setPool(Pool pool) {
        this.pool = pool;
    }

    public Lifeguard getLifeguard() {
        return lifeguard;
    }

    public void setLifeguard(Lifeguard lifeguard) {
        this.lifeguard = lifeguard;
    }

    public LocalTime getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(LocalTime timeIn) {
        this.timeIn = timeIn;
    }

    public LocalTime getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(LocalTime timeOut) {
        this.timeOut = timeOut;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    @Override
    public String toString() {
        return lifeguard + " at " + pool + timeIn + " - " + timeOut + " on " + date;
    }
}
