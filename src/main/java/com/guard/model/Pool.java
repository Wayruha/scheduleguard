package com.guard.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Set;

@Entity
@Table(name = "\"Pool\"")
public class Pool {

    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String address;

    @Column(name = "lg_count", nullable = false)
    private int lgCount = 1;

    @ManyToOne
    @JoinColumn(name = "area_id")
    private Area area;

    @JsonIgnore
    @OneToMany(mappedBy = "pool",fetch = FetchType.LAZY)
    private Set<Assign> assignSet;

    @Column(name = "mon_start", nullable = false)
    private LocalTime monStart;
    @Column(name = "mon_end", nullable = false)
    private LocalTime monEnd;
    @Column(name = "tue_start", nullable = false)
    private LocalTime tueStart;
    @Column(name = "tue_end", nullable = false)
    private LocalTime tueEnd;
    @Column(name = "wed_start", nullable = false)
    private LocalTime wedStart;
    @Column(name = "wed_end", nullable = false)
    private LocalTime wedEnd;
    @Column(name = "thu_start", nullable = false)
    private LocalTime thuStart;
    @Column(name = "thu_end", nullable = false)
    private LocalTime thuEnd;
    @Column(name = "fri_start", nullable = false)
    private LocalTime friStart;
    @Column(name = "fri_end", nullable = false)
    private LocalTime friEnd;
    @Column(name = "sat_start", nullable = false)
    private LocalTime satStart;
    @Column(name = "sat_end", nullable = false)
    private LocalTime satEnd;
    @Column(name = "sun_start", nullable = false)
    private LocalTime sunStart;
    @Column(name = "sun_end", nullable = false)
    private LocalTime sunEnd;


    public Pool() {
    }

    public Set<Assign> getAssignSet() {
        return assignSet;
    }

    public void setAssignSet(Set<Assign> assignSet) {
        this.assignSet = assignSet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getLgCount() {
        return lgCount;
    }

    public void setLgCount(int lgCount) {
        this.lgCount = lgCount;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public LocalTime getMonStart() {

        return monStart;
    }

    public void setMonStart(LocalTime monStart) {
        this.monStart = monStart;
    }

    public LocalTime getMonEnd() {
        return monEnd;
    }

    public void setMonEnd(LocalTime monEnd) {
        this.monEnd = monEnd;
    }

    public LocalTime getTueStart() {
        return tueStart;
    }

    public void setTueStart(LocalTime tueStart) {
        this.tueStart = tueStart;
    }

    public LocalTime getTueEnd() {
        return tueEnd;
    }

    public void setTueEnd(LocalTime tueEnd) {
        this.tueEnd = tueEnd;
    }

    public LocalTime getWedStart() {
        return wedStart;
    }

    public void setWedStart(LocalTime wedStart) {
        this.wedStart = wedStart;
    }

    public LocalTime getWedEnd() {
        return wedEnd;
    }

    public void setWedEnd(LocalTime wedEnd) {
        this.wedEnd = wedEnd;
    }

    public LocalTime getThuStart() {
        return thuStart;
    }

    public void setThuStart(LocalTime thuStart) {
        this.thuStart = thuStart;
    }

    public LocalTime getThuEnd() {
        return thuEnd;
    }

    public void setThuEnd(LocalTime thuEnd) {
        this.thuEnd = thuEnd;
    }

    public LocalTime getFriStart() {
        return friStart;
    }

    public void setFriStart(LocalTime friStart) {
        this.friStart = friStart;
    }

    public LocalTime getFriEnd() {
        return friEnd;
    }

    public void setFriEnd(LocalTime friEnd) {
        this.friEnd = friEnd;
    }

    public LocalTime getSatStart() {
        return satStart;
    }

    public void setSatStart(LocalTime satStart) {
        this.satStart = satStart;
    }

    public LocalTime getSatEnd() {
        return satEnd;
    }

    public void setSatEnd(LocalTime satEnd) {
        this.satEnd = satEnd;
    }

    public LocalTime getSunStart() {
        return sunStart;
    }

    public void setSunStart(LocalTime sunStart) {
        this.sunStart = sunStart;
    }

    public LocalTime getSunEnd() {
        return sunEnd;
    }

    public void setSunEnd(LocalTime sunEnd) {
        this.sunEnd = sunEnd;
    }

    @Override
    public String toString() {
        return "Pool["+ id +"] "+name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pool)) return false;

        Pool pool = (Pool) o;

        return id == pool.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
