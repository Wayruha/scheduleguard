package com.guard.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "\"Area\"")
public class Area {
    @Id
    @GeneratedValue
    private int id;
    @Column
    private String name;
    @Column//(columnDefinition = "char",length = 2)
    private String state; //TODO enumeration?
    @OneToOne
    @JoinColumn(name = "id")
    private Employee supervisor;

    @JsonIgnore
    @OneToMany (mappedBy="area")
  //  @JoinColumn(name = "area_id")
    private Set<Lifeguard> lifeguards;

    public Area() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Employee getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Employee supervisor) {
        this.supervisor = supervisor;
    }
}
