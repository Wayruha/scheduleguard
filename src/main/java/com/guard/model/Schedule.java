package com.guard.model;

import com.guard.enums.Days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class Schedule<T> {
    private List<List<Assign>> schedule;

    //for each assign entry get the corresponding list in week list and put the assign there
    public Schedule(List<Assign> list) {
        this();
        Calendar calendar=Calendar.getInstance();
        List<Assign> dayOfWeekList;
        for(Assign assign:list){
            calendar.setTime(assign.getDate());
            //sunday is #1, so we convert it in order to have  monday #0

            dayOfWeekList=schedule.get((calendar.get(Calendar.DAY_OF_WEEK)+7-2)%7);
            dayOfWeekList.add(assign);
        }
    }

    //initialize our arrayList
    public Schedule() {
        schedule=new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            schedule.add(new ArrayList<>());
        }
    }

    public static double calculateHoursAmount(Schedule schedule){
        double amount=0;
        for(Days day:Days.values())
            for(Object obj : schedule.getScheduleOnDay(day)){
                Assign assign= (Assign) obj;
                amount+=assign.getHoursCount();
            }
        return amount;
    }

    public List<Assign> getScheduleOnDay(Days day){
        return schedule.get(day.ordinal());
    }

    public void setScheduleOnDay(Days day, Assign... assigns){
        setScheduleOnDay(day, Arrays.asList(assigns));
    }

    public void setScheduleOnDay(Days day, List<Assign> assigns){
        schedule.set(day.ordinal(),assigns);
    }

    public List<List<Assign>> getSchedule() {
        return schedule;
    }




    @Override
    public String toString() {
        StringBuilder bldr=new StringBuilder();
        for (int i = 0; i < Days.values().length; i++) {
            List<Assign> asns=getScheduleOnDay(Days.values()[i]);
            bldr.append("[");
            bldr.append(Days.values()[i].getShortRepresentation());
            bldr.append("]:");
            for (Assign asn:asns){
                bldr.append(asn.getPool().getName());
                bldr.append("from ");
                bldr.append(asn.getTimeIn());
                bldr.append(" to ");
                bldr.append(asn.getTimeOut());
            }
            bldr.append("; \n");
        }

        return bldr.toString();
    }
}
