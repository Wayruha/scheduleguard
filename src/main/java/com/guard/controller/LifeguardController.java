package com.guard.controller;

import com.guard.dao.AreaDao;
import com.guard.dao.AssignDao;
import com.guard.dao.LifeguardDao;
import com.guard.model.Area;
import com.guard.model.Lifeguard;
import com.guard.model.Schedule;
import com.guard.validatingForm.LifeguardForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/lifeguards")
public class LifeguardController {
    @Autowired
    LifeguardDao lgDao;
    @Autowired
    AssignDao asnDao;
    @Autowired
    AreaDao areaDao;

    //private final Logger logger =  LoggerFactory.getLogger(this.getClass());

    Area area;
    Date weekStart;

    {
        area = null;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.DATE, 27);
        weekStart = calendar.getTime();
    }

    //TODO передавати АреаІд і тиждень в параметрах запиту
    @RequestMapping()
    public ModelAndView mainHandler() {
        ModelAndView mav = new ModelAndView("lifeguardsPage");
        List<Lifeguard> lifeguardList = lgDao.getAllForArea(area);
        List<Schedule<Lifeguard>> schedules = new ArrayList<>();
        for (Lifeguard lg : lifeguardList) {
            Schedule<Lifeguard> schedule = asnDao.getScheduleForLifeguard(lg, weekStart);
            schedules.add(schedule);
            lg.setHoursPerCurrentWeek(Schedule.calculateHoursAmount(schedule));
        }
        //   logger.debug("[LifeguardController] has " + schedules.size() + " schedules and " + lifeguardList.size() + " lgs");
        mav.addObject("lifeguards", lifeguardList);//TODO тут дублювання даних. Можна ЛГсів прибрати і оставити тілкьи скедьюли
        mav.addObject("schedules", schedules);
        return mav;
    }

    @GetMapping(value = "/edit")
    public String createUpdateGetPage(@RequestParam(value = "id", required = false, defaultValue = "-1") int id, Model model) {
        LifeguardForm lgForm;
        //   logger.warn("Logger is okay. Debug level");
        System.out.println("перед цим має бути повідомлення логера");
        if (id == -1) lgForm = new LifeguardForm();
        else
            lgForm = new LifeguardForm(lgDao.getById(id));
        model.addAttribute("lifeguardForm", lgForm);
        return "lifeguardData";
    }

    @PostMapping(value = "/edit.do")
    public String processCreateUpdate(@Valid @ModelAttribute("lifeguardForm") LifeguardForm lgForm, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
//            Lifeguard lg=new Lifeguard(lgForm,areaDao.getById(lgForm.getArea()));
            //lgDao.save(lg)
            return "success";
        } else {
            System.out.println("validation had errors");
        }
        return "lifeguardData";
    }

    @PostMapping(value = "/delete")
    public String deleteLifeguard(@RequestParam(value="id") int id){
        lgDao.deleteById(id);
        return "success";
    }

    @RequestMapping(value = "/get/{id}")
    @ResponseBody
    public Lifeguard getLifeguard(@PathVariable(value = "id") int id){
        Lifeguard lg= lgDao.getById(id);
        // logger.debug(lg.toString());
        return lg;

    }


}
