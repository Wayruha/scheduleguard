package com.guard.controller;

import com.guard.dao.AreaDao;
import com.guard.dao.AssignDao;
import com.guard.dao.LifeguardDao;
import com.guard.dao.PoolDao;
import com.guard.model.Area;
import com.guard.model.Lifeguard;
import com.guard.model.Pool;
import com.guard.model.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/schedule")
public class SceduleController {

    @Autowired
    AssignDao asnDao;
    @Autowired
    LifeguardDao lgDao;
    @Autowired
    AreaDao areaDao;
    @Autowired
    PoolDao poolDao;

    //private final Logger logger = LoggerFactory.getLogger(this.getClass());

    //TODO передавати АреаІд і тиждень в параметрах запиту
    @RequestMapping(value = "")
    public ModelAndView getScheduleForArea(Area area, Date startOfWeek) {
        ModelAndView mav=new ModelAndView();
        List<Pool> pools;
        List<Schedule<Pool>> schedules = new ArrayList<>();
        pools = poolDao.getAllForArea(area);
        for (Pool pool : pools) {
            schedules.add(asnDao.getScheduleForPool(pool, startOfWeek));
        }
        mav.addObject("schedules", schedules);
        return mav;
    }


    @RequestMapping(value = "/getLifeguards/{areaId}")
    @ResponseBody
    public List<Lifeguard> getLifeguard(@PathVariable(value = "areaId") int areaId) {
        List<Lifeguard> lifeguards;
        if (areaId == 0)
            lifeguards = lgDao.getAll();
        else
            lifeguards = lgDao.getAllForArea(areaId);
        return lifeguards;
    }


}
