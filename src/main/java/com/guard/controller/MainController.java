package com.guard.controller;

import com.guard.dao.AssignDao;
import com.guard.dao.LifeguardDao;
import com.guard.enums.Days;
import com.guard.model.Assign;
import com.guard.model.Lifeguard;
import com.guard.model.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "")
public class MainController {
    @Autowired
    LifeguardDao lgDao;
    @Autowired
    AssignDao asnDao;

    @RequestMapping(value = "")
    public ModelAndView mainHandler() {
        ModelAndView mav = new ModelAndView("main");

        return mav;
    }

    @RequestMapping(value = "/lifeguardsTest")
    public ModelAndView lifeguardsHandler() {
        ModelAndView mav = new ModelAndView("main");
        System.out.println("Main handler in Main controller was called");
        Lifeguard lg = new Lifeguard();
        lg.setName("testLg");
        lg.setAppartments("dalekoTest");
        lg.setCountry("ukraine");
        lgDao.save(lg);
        System.out.println("saving was successfull");
        return mav;
    }

    @RequestMapping(value = "/lifeguards/{id}")
    public ModelAndView scheduleHandler(@PathVariable(value = "id") int id) {
        return null;
    }

}
