<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <meta charset="utf-8">
    <title>Lifeguard</title>
</head>

<body>
<h1>Дарова</h1>
<h2>${schedules}</h2>
<div>
    <table>
        <tr>
            <td width="70%">
                <c:forEach items="${schedules}" var="schedule">
                    <p><font size="6">${schedule}</font></p>
                </c:forEach>
            </td>
            <td>
                <c:forEach items="${lifeguards}" var="lg">
                    <p><font size="7">${lg}:${lg.hoursPerCurrentWeek}</font></p>
                </c:forEach>
            </td>
        </tr>
    </table>

</div>
</body>

</html>