<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <meta charset="utf-8">
    <title>Lifeguard</title>
</head>

<body>
<h1>Дарова</h1>
<h2>${schedules}</h2>
<div>
    <springForm:form method="POST" commandName="lifeguardForm"
                     action="edit.do">
        <table>
            <tr>
                <td>Name:</td>
                <td><springForm:input path="name" /></td>
                <td><springForm:errors path="name" cssClass="error" /></td>
            </tr>
            <tr>
                <td>Country:</td>
                <td><springForm:input path="country" /></td>
                <td><springForm:errors path="country" cssClass="error" /></td>
            </tr>
            <tr>
                <td>Appartments:</td>
                <td><springForm:input path="appartments" /></td>
                <td><springForm:errors path="appartments" cssClass="error" /></td>
            </tr>
            <tr>
                <td>Area:</td>
                <td><springForm:input path="area" /></td>
                <td><springForm:errors path="area" cssClass="error" /></td>
            </tr>
            <tr>
                <td colspan="3"><input type="submit" value="Save lifeguard"></td>
            </tr>
        </table>

    </springForm:form>

</div>
</body>

</html>