package com.guard.dao;


import com.guard.model.Lifeguard;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@Rollback
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class LifeguardDaoTest {
    @Autowired
    private LifeguardDao lgDao;


    @Before
    public void setUp() throws Exception {
        System.out.println("Setting up -------");
        for (Lifeguard lifeguard : lgDao.getAll()) {
            System.out.println(lifeguard);
        }
    }

    @Test
    public void selectTest() {
        int size = lgDao.getAll().size();
        Assert.assertEquals(3, size);
        final int id = 1, areaId = 1;
        String name = "Roman Diachuk";
        Lifeguard lg = lgDao.getById(id);
        Assert.assertEquals(id, lg.getId());
        Assert.assertEquals(name, lg.getName());
        Assert.assertEquals(areaId, lg.getArea().getId());
    }

    @Test
    public void getByNameTest() {
        String name = "Roman Diachuk";
        int defId = 1;
        Lifeguard lg = lgDao.getByName(name);
        Assert.assertEquals(defId, lg.getId());
    }

    @Test
    public void getAllForArea() {
        final int areaId = 1;
        List lifeguards = lgDao.getAllForArea(areaId);


        Assert.assertEquals(2, lifeguards.size());
    }


    @Test
    public void updateLgTest() {
        int id = 1;
        String newName = "AlmostRomanName";
        String newCountry = "USA";

        Lifeguard romanLg = lgDao.getById(id);

        romanLg.setName(newName);
        romanLg.setCountry(newCountry);

        lgDao.updateRecord(romanLg);

        Lifeguard newRomanLg = lgDao.getById(id);
        Assert.assertEquals(newName, newRomanLg.getName());
        Assert.assertEquals(newCountry, newRomanLg.getCountry());
    }

    @Test
    public void addLg() throws Exception {
        Lifeguard lg = new Lifeguard();
        lg.setName("testLg");
        lg.setAppartments("dalekoTest");
        lg.setCountry("ukraine");
        lgDao.save(lg);

        Lifeguard lg2 = lgDao.getByName(lg.getName());
        Assert.assertEquals(lg.toString(), lg2.toString());
    }

    @Test
    public void deleteLg() throws Exception {
        int id = 3;  //it has only 3 lines
        Lifeguard lg = lgDao.getById(id);
        lgDao.delete(lg);
        try {
            lgDao.getById(id);
        } catch (Exception ex) {
            System.out.println("no result");
            assert (true);
        }
    }


}