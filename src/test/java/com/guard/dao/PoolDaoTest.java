package com.guard.dao;

import com.guard.model.Pool;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest

public class PoolDaoTest {

    @Autowired
    PoolDao poolDao;
    @Autowired
    TestEntityManager entityManager;

    @Test
    public void save(){
        Pool pool=new Pool();
        pool.setName("testPool");
        pool.setAddress("42,Albee lane");
        entityManager.persist(pool);

        Pool pool1 = poolDao.getByName(pool.getName());

        Assert.assertEquals(pool.getId(),pool1.getId());
    }
}
