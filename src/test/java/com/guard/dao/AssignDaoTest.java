package com.guard.dao;

import com.guard.configuration.GuardApplication;
import com.guard.enums.Days;
import com.guard.model.Assign;
import com.guard.model.Lifeguard;
import com.guard.model.Schedule;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@DataJpaTest
@Rollback
public class AssignDaoTest {

    @Autowired
    private AssignDao asnDao;

    @Before
    public void setUp() throws Exception {

    }


    @Test
    public void getAssignForWeekTest() throws ParseException {
        final String expectedPattern = "dd.MM.yyyy";
        final String userInput = "27.02.2017";
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        final Date date = formatter.parse(userInput);
        final int[] expPoolId = new int[]{2, 1, 1, 2}, expHours = new int[]{8, 8,4,5};
        int counter = 0;

        final int id = 1;
        Lifeguard lg=new Lifeguard();
        lg.setId(id);
        Schedule<Lifeguard> schedule = asnDao.getScheduleForLifeguard(lg, date);

        boolean upToDateDBFlag=!schedule.getScheduleOnDay(Days.MONDAY).isEmpty()
                &&!schedule.getScheduleOnDay(Days.TUESDAY).isEmpty()
                &&!schedule.getScheduleOnDay(Days.WEDNESDAY).isEmpty();
        Assert.assertEquals(true ,upToDateDBFlag);

        for (int i = 0; i < Days.values().length; i++) {
            List<Assign> asns = schedule.getScheduleOnDay(Days.values()[i]);
            for (Assign asn : asns) {
                Assert.assertEquals(expPoolId[counter], asn.getPool().getId());
                Assert.assertEquals(expHours[counter++], asn.getTimeOut().getHour()-asn.getTimeIn().getHour());
            }

        }
    }

    private int convertDateToDayOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DATE);
    }

}