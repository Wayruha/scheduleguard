FROM frolvlad/alpine-oraclejdk8:slim
VOLUME /tmp
COPY src/main/resources /src/main/resources
COPY src/main/webapp /src/main/webapp
ADD target/ScheduleGuard.jar app.jar
RUN sh -c 'touch app.jar'
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]